
![Branch master](https://img.shields.io/badge/branch-master-brightgreen.svg?style=flat-square)    [![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://raw.githubusercontent.com/detailyang/awesome-cheatsheet/master/LICENSE)

## About
simple jekyll based from H20 . full dark

###### Demo
- host github : https://rokhimin.github.io/blog
- host heroku : https://jekyll-rokhimin.herokuapp.com

## Deploy

###### Github
Settings > GithubPages > Select Branch

###### Heroku
[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://dashboard.heroku.com/new?button-url=https://github.com/rokhimin/blog/tree/deploy_heroku&template=https://github.com/rokhimin/blog/tree/deploy_heroku) 

## Test Locally
- Require (ruby, bundler, jekyll)
- `bundle install`
- `bundle exec jekyll serve --watch`

###### Thankyou :)

